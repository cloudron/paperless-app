# Paperless on Cloudron

Scan, index, and archive all of your paper documents

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=io.cloudron.paperless)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id io.cloudron.paperless
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
git clone https://git.cloudron.io/cloudron/paperless-app.git && cd <repo>

cloudron build
cloudron install
```

## Usage

Once installed, refer to the post install instructions for further details.